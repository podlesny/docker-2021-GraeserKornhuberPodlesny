#!/usr/bin/env bash

set -e

tag=2021-GraeserKornhuberPodlesny
docker build \
    -t podlesny/dune-tectonic-eval:${tag} 03-evaluate

tmpdir=`mktemp -d`
docker run --rm -v $tmpdir:/bridge podlesny/dune-tectonic-eval:${tag} \
    sh -c 'tar -C /dune-build/dune-tectonic/src/multi-body-problem/output -rf /bridge/generated.tar \
               2-body/ \
               5-body/'
mv $tmpdir/generated.tar .
rmdir $tmpdir
rm -rf generated
mkdir generated
tar -xvf generated.tar -C generated
rm generated.tar
