#!/usr/bin/env bash

set -e

tag=2021-GraeserKornhuberPodlesny
docker build \
    -t podlesny/dune-tectonic:${tag} 02-dune-tectonic
